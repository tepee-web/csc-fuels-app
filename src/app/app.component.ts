import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { GoogleMapsService } from '../providers/google-maps';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = HomePage;

    constructor(
        private googleMaps: GoogleMapsService,
        private platform: Platform,
        private statusBar: StatusBar,
        private splashScreen: SplashScreen
    ) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.show();
            this.statusBar.styleLightContent();
            this.statusBar.backgroundColorByHexString('#000000');
            this.statusBar.overlaysWebView(true);
            this.googleMaps.init();
        });
    }

    public ngAfterViewInit() {
        this.platform.ready().then(() => {
            this.splashScreen.hide();
            this.detectNotch();
        });
    }

    detectNotch() {
        const mql = window.matchMedia('(env(--safe-area-inset-top))');
        if (mql.matches || window.innerHeight / window.innerWidth > 2) {
          document.body.classList.add('has-notch');
        }
    }
}

