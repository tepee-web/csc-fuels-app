import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { GoogleMap } from '../components/google-maps';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { HomePage } from '../pages/home/home';
import { RoutePlannerPage } from '../pages/route-planner/route-planner';
import { SiteSinglePage } from '../pages/site-single/site-single';
import { SitesNearbyPage } from '../pages/sites-nearby/sites-nearby';
import { FuelCardsService } from '../providers/fuel-cards';
import { GeolocationService } from '../providers/geolocation';
import { GoogleMapsService } from '../providers/google-maps';
import { SearchAreaService } from "../providers/search-area";
import { MyApp } from './app.component';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Geolocation } from "@ionic-native/geolocation";
import { Network } from "@ionic-native/network";
import { OfflineAlert } from "../components/offline-alert";
import { AppVersion } from "@ionic-native/app-version";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        SitesNearbyPage,
        RoutePlannerPage,
        ContactUsPage,
        SiteSinglePage,
        GoogleMap,
        OfflineAlert
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        SitesNearbyPage,
        RoutePlannerPage,
        ContactUsPage,
        SiteSinglePage,
        GoogleMap,
    ],
    providers: [
        GeolocationService,
        GoogleMapsService,
        SearchAreaService,
        FuelCardsService,
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        LaunchNavigator,
        Geolocation,
        Network,
        AppVersion
    ]
})
export class AppModule { }
