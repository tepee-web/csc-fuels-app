import { Component, ElementRef } from "@angular/core";
import MarkerClusterer from '@google/markerclusterer';
import { Subject } from "rxjs/Subject";
import { GoogleMapsService } from "../providers/google-maps";
import { GeolocationService } from "../providers/geolocation";
import { Subscription } from "rxjs/Subscription";
import AutocompletionRequest = google.maps.places.AutocompletionRequest;
import ComponentRestrictions = google.maps.places.ComponentRestrictions;
import Place = google.maps.Place;
import MapOptions = google.maps.MapOptions;


@Component({
    selector: '[google-map]',
    template: '',
})
export class GoogleMap {

    private clusteringEnabled: boolean = false;

    private isReady: boolean = false;
    private readyCallbacks: Function[] = [];

    private clusterer: MarkerClusterer;
    private map: google.maps.Map;
    private markers: google.maps.Marker[] = [];
    private startPointMarker: google.maps.Marker;
    private endPointMarker: google.maps.Marker;
    private positionMarker: google.maps.Marker;
    private directionService: google.maps.DirectionsService;
    private placesService: google.maps.places.PlacesService;
    private autocompleteService: google.maps.places.AutocompleteService;
    private onPositionChanged: Subscription;

    public boundsChanged = new Subject<google.maps.LatLngBounds>();
    public tilesLoaded = new Subject<void>();
    public route: google.maps.Polyline;
    public searchArea: google.maps.Polygon;

    public MAP_TYPES = {
        ROAD: 'road',
        SATELLITE: 'satellite',
        HYBRID: 'hybrid',
    };

    constructor(
        private elementRef: ElementRef,
        private geolocation: GeolocationService,
        private googleMaps: GoogleMapsService,
    ) {

    }

    public ngAfterViewInit() {
        this.tilesLoaded.subscribe(() => {
            this.isReady = true;

            for (let callback of this.readyCallbacks) {
                callback();
            }
        });

        this.googleMaps.ready(() => {
            this.map = this.createMap();

            this.directionService = new google.maps.DirectionsService();
            this.placesService = new google.maps.places.PlacesService(this.map);
            this.autocompleteService = new google.maps.places.AutocompleteService();

            this.addMarkers();
        });
    }

    private addMarkers() {
        for (let marker of this.markers) {
            if (this.clusteringEnabled) {
                this.clusterer.addMarker(marker)
            } else marker.setMap(this.map);
        }
    }

    public addMarker(options: google.maps.MarkerOptions): google.maps.Marker;
    public addMarker(marker: google.maps.Marker): google.maps.Marker;
    public addMarker(input): google.maps.Marker {
        let marker: google.maps.Marker;

        if (input instanceof google.maps.Marker) {
            marker = input;
        } else {
            let options: google.maps.MarkerOptions = input;
            marker = this.createMarker(options)
        }

        this.markers.push(marker);

        if (this.map) {
            if (this.clusteringEnabled) this.clusterer.addMarker(marker)
            else marker.setMap(this.map);
        }

        return marker;
    }

    public clearMarker(marker: google.maps.Marker) {
        let index = this.markers.indexOf(marker);

        if (index !== -1) {
            this.markers.splice(index, 1);
            marker.unbindAll();
        }

        if (this.clusteringEnabled) this.clusterer.removeMarker(marker);
        else marker.setMap(null);

        marker = null;
    }

    public clearMarkers() {
        for (let marker of this.markers) {
            this.clearMarker(marker);
        }
        this.markers = [];
    }

    public clearStartEndMarkers() {
        if (this.startPointMarker !== undefined) this.startPointMarker.setMap(null);
        if (this.endPointMarker !== undefined) this.endPointMarker.setMap(null);
    }

    public createMap(): google.maps.Map {
        let map = new google.maps.Map(this.elementRef.nativeElement, {
            center: { lat: 54.50211, lng: -6.1050857 },
            fullscreenControl: false,
            mapTypeControl: false,
            minZoom: 9,
            streetViewControl: false,
            zoomControl: false,
            zoom: 10,
        });

        map.addListener('bounds_changed', () => this.boundsChanged.next(this.getBounds()));
        map.addListener('tilesloaded', () => this.tilesLoaded.next());

        return map;
    }

    public createMarker(options: google.maps.MarkerOptions): google.maps.Marker {
        return new google.maps.Marker(options);
    }

    public disableClustering() {
        this.setClusteringEnabled(false);
    }

    public enableClustering() {
        this.setClusteringEnabled(true);
    }

    public getBounds(): google.maps.LatLngBounds {
        return this.map.getBounds();
    }

    public ready(): Promise<void> {
        if (this.isReady) return Promise.resolve();

        return new Promise<void>(resolve => {
            this.readyCallbacks.push(resolve);
        });
    }

    public setCenter(center: google.maps.LatLng | google.maps.LatLngLiteral) {
        this.map.setCenter(center);
    }

    public setZoom(zoom: number) {
        this.map.setZoom(zoom);
    }

    public setClusteringEnabled(enabled: boolean) {
        this.clusteringEnabled = enabled;

        if (this.clusteringEnabled) {
            this.clusterer = new MarkerClusterer(this.map);

            let sizes = [22, 23, 27, 31, 37];
            let shadowSize = 10;
            let styles = this.clusterer.getStyles();

            for (let key in styles) {
                let style = styles[key];
                let size = sizes[key];

                style.anchor = [((8 / 22) * size), null];
                style.backgroundPosition = 'center center';
                style.height = size + shadowSize + style.anchor[0];
                style.textColor = '#FFFFFF';
                style.textSize = 11;
                style.url = '/assets/imgs/marker-clusterer/m' + (parseInt(key) + 1) + '.svg',
                style.width = size + shadowSize;
            }

            this.clusterer.setStyles(styles);

            for (let marker of this.markers) {
                marker.setMap(null);
                this.clusterer.addMarker(marker);
            }
        } else {
            for (let marker of this.markers) {
                this.clusterer.removeMarker(marker);
                marker.setMap(this.map);
            }

            this.clusterer = null;
        }
    }

    public async startWatchingPosition() {
        let position = await this.geolocation.getCurrentPosition();

        let marker = new google.maps.Marker({
            icon: '/assets/imgs/map-marker-user.svg',
            anchorPoint: new google.maps.Point(11, 32),
            map: this.map,
            position: {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            },

            title: 'Current Position',
        });

        this.positionMarker = marker;

        this.onPositionChanged = this.geolocation.watchPosition().subscribe(position => {
            this.positionMarker.setPosition({
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            });
        }, error => {
            this.stopWatchingPosition();
        });
    }

    public stopWatchingPosition() {
        this.onPositionChanged.unsubscribe();
        this.positionMarker.unbindAll();
        this.positionMarker.setMap(null);
        this.positionMarker = null;
    }

    public async toggleWatchingPosition(): Promise<boolean> {
        if (!this.onPositionChanged || this.onPositionChanged.closed) {
            await this.startWatchingPosition();
            return true;
        } else {
            this.stopWatchingPosition();
            return false;
        }
    }

    public async generateRoute(startLocation: string|Place, endLocation: string|Place) {
        return new Promise((resolve, reject) => {
            const directionsRequest = {
                origin: startLocation,
                destination: endLocation,
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL,
                waypoints: [],
                optimizeWaypoints: true,
                provideRouteAlternatives: false,
            };

            this.directionService.route(directionsRequest, (result, status) => {
                if (status == google.maps.DirectionsStatus.OK) {
                    const waypoints = result.routes[0].overview_path;

                    this.route = new google.maps.Polyline({
                        path: waypoints.map(location => ({lat: location.lat(), lng: location.lng()})),
                        strokeColor: '#0099FF',
                        strokeOpacity: 0.8,
                        strokeWeight: 3
                    });

                    const [startDestination] = waypoints.slice(0, 1);
                    const [endDestination] = waypoints.slice(-1);

                    this.startPointMarker = this.createMarker({
                        icon: '/assets/imgs/pointer-icon.svg',
                        position: {
                            lat: startDestination.lat(),
                            lng: startDestination.lng()
                        },
                    });
                    this.startPointMarker.setMap(this.map);

                    this.endPointMarker = this.createMarker({
                        icon: '/assets/imgs/pointer-icon.svg',
                        position: {
                            lat: endDestination.lat(),
                            lng: endDestination.lng()
                        },
                    });
                    this.endPointMarker.setMap(this.map);

                    this.route.setMap(this.map);

                    const bounds = new google.maps.LatLngBounds();
                    this.route
                        .getPath()
                        .forEach((latLng: google.maps.LatLng) => bounds.extend(latLng));

                    this.map.fitBounds(bounds);

                    resolve(this.route);
                }

                return reject();
            });
        });
    }

    public placePrediction(phrase: string) {
        const RESTRICTIONS: ComponentRestrictions = {
            country: ['GB', 'IE']
        };

        const OPTIONS: AutocompletionRequest = {
            input: phrase,
            componentRestrictions: RESTRICTIONS
        };

        return new Promise((resolve) => {
            this.autocompleteService.getPlacePredictions(OPTIONS, (predictions, status) => {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    return resolve(predictions.map(prediction => ({
                        name: prediction.structured_formatting['main_text'],
                        address: prediction.description,
                        place_id: prediction.place_id
                    })));
                }
            });
        });
    }

    public getLatLngForPlace(place: string): Promise<google.maps.LatLng> {
        return new Promise(resolve => {
            this.placesService.textSearch({query: place}, (response, status) => {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    return resolve(new google.maps.LatLng(response[0].geometry.location.lat(), response[0].geometry.location.lng()));
                }
            });
        });
    }

    public setOptions(options) {
        this.map.setOptions(options);
    }

    public displaySatelliteView(): void {
        this.map.setOptions({
            mapTypeId: google.maps.MapTypeId.SATELLITE
        });
    }

    public displayHybridView(): void {
        this.map.setOptions({
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
    }

    public displayRoadView(): void {
        this.map.setOptions({
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    }
}
