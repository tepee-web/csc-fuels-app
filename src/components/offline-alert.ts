import { Component } from '@angular/core';
import { Network } from "@ionic-native/network";
import { AlertController, Alert } from "ionic-angular";
import { Subscription } from "rxjs";

/**
 * Generated class for the OfflineAlert component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'offline-alert',
    template: ''
})
export class OfflineAlert {
    private disconnectSubscription: Subscription;
    private connectionSubscription: Subscription;

    public offlineMessage: Alert = null;


    constructor(private network: Network, private alert: AlertController) {
    }

    public ngOnInit() {
        this.disconnectSubscription = this.network.onDisconnect().subscribe(() => this.showOfflineMessage());
        this.connectionSubscription = this.network.onConnect().subscribe(() => this.hideOfflineMessage());
    }

    public ngOnDestroy() {
        this.disconnectSubscription.unsubscribe();
        this.connectionSubscription.unsubscribe();
    }

    public showOfflineMessage() {
        if (this.offlineMessage) this.hideOfflineMessage();

        this.offlineMessage = this.alert.create({
            title: 'No internet connection',
            subTitle: 'App will resume when an internet connection is established',
            enableBackdropDismiss: false
        });

        this.offlineMessage.present();
    }

    public hideOfflineMessage() {
        this.offlineMessage.dismiss();
    }
}
