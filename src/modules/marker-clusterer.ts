
declare module '@google/markerclusterer' {
    import * as MarkerClusterer from '@google/markerclusterer/src/markerclusterer';

    interface MarkerClusterOptions {
        /**
         * The grid size of a cluster in pixels.
         */
        gridSize?: number;

        /**
         * The maximum zoom level that a marker can be part of a cluster.
         */
        maxZoom?: number;

        /**
         * Whether the default behaviour of clicking on a cluster is to zoom into it.
         */
        zoomOnClick?: boolean;

        /**
         * The base URL where the images representing clusters will be found.
         * The full URL will be: imagePath}[1-5].{imageExtension}
         *
         * Default: '../images/m'.
         */
        imagePath?: string;

        /**
         * The suffix for images URL representing clusters will be found. See _imagePath_ for details.
         *
         * Default: 'png'.
         */
        imageExtension?: string;

        /**
         * Whether the center of each cluster should be the average of all markers in the cluster.
         */
        averageCenter?: boolean;

        /**
         * The minimum number of markers to be in a cluster before the markers are hidden and a count is shown.
         */
        minimumClusterSize?: number;

        /**
         * An object that has style properties
         */
        styles?: MarkerClusterStyles[];
    }

    interface MarkerClusterStyles {
        /**
         *  The image url.
         */
        url?: string;

        /**
         *  The image height.
         */
        height?: number;

        /**
         *  The image width.
         */
        width?: number;

        /**
         *  The anchor position of the label text.
         */
        anchor?: any;

        /**
         *  The text color.
         */
        textColor?: string;

        /**
         *  The text size.
         */
        textSize?: number;

        /**
         *  The position of the backgound x, y.
         */
        backgroundPosition?: string;
    }

    export default class MarkerClusterer {
        /**
         * A Marker Clusterer that clusters markers.
         *
         * @param map The Google map to attach to.
         * @param markers Optional markers to add to the cluster.
         * @param options An object that has clusterer options.
         * @extends google.maps.OverlayView
         */
        constructor(map: google.maps.Map, markers?: google.maps.Marker[], options?: MarkerClusterOptions);

        /**
         * Adds a marker to the clusterer and redraws if needed.
         *
         * @param marker The marker to add.
         * @param nodraw Whether to redraw the clusters.
         */
        addMarker(marker: google.maps.Marker, nodraw?: boolean): void;
        addMarkers(markers: google.maps.Marker[], nodraw?: boolean): void;
        clearMarkers(): void;
        getCalculator(): Function;
        getExtendedBounds(bounds: google.maps.LatLngBounds);
        getGridSize();
        getMap();
        getMarkers();
        getMaxZoom();
        getStyles(): MarkerClusterStyles[];
        getTotalClusters();
        getTotalMarkers();
        isZoomOnClick();
        redraw(): void;
        removeMarker(marker: google.maps.Marker);
        resetViewport(): void;
        setCalculator(calculator: Function): void;
        setGridSize(size: number): void;
        setMap(map: google.maps.Map): void;
        setMaxZoom(maxZoom: number): void;
        setStyles(styles: MarkerClusterStyles[]): void;
    }
}
