import { Component } from '@angular/core';
import { Alert, AlertController, NavController, Platform } from 'ionic-angular';
import { SitesNearbyPage } from '../sites-nearby/sites-nearby';
import { RoutePlannerPage } from '../route-planner/route-planner';
import { ContactUsPage } from '../contact-us/contact-us';
import { Network } from "@ionic-native/network";
import { AppVersion } from "@ionic-native/app-version";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    private offlineMessage: Alert;
    public version: string | number = '';
    public build: string | number = '';

    constructor(
        public navCtrl: NavController,
        private network: Network,
        private alert: AlertController,
        private appVersion: AppVersion,
        private platform: Platform
    ) {

    }

    public async ionViewDidLoad() {
        const setBuildDetails = async () => {
            try {
                this.version = await this.getAppVersion();
                this.build = await this.getBuild();
            } catch (e) {
                // most likely running in a dev environment
                this.version = '9.9.9';
                this.build = 99;
            }
        };

        this.platform.ready().then(setBuildDetails);
    }

    public hideOfflineMessage(): void {
        this.offlineMessage.dismiss();
    }

    public goToRoutePlanner(): void {
        if (this.isOnline()) {
            this.navCtrl.push(RoutePlannerPage, {});
        } else {
            this.showOfflineAlert();
        }
    }

    public goToContactUs(): void {
        this.navCtrl.push(ContactUsPage, {});
    }

    public isOnline(): boolean {
        let connection = this.network.type;

        return [
            this.network.Connection.CELL,
            this.network.Connection.CELL_2G,
            this.network.Connection.CELL_3G,
            this.network.Connection.CELL_4G,
            this.network.Connection.WIFI
        ].indexOf(connection) !== -1;
    }

    public goToSitesNearby(): void {
        if (this.isOnline()) {
            this.navCtrl.push(SitesNearbyPage, {});
        } else {
            this.showOfflineAlert();
        }
    }

    private showOfflineAlert(): void {
        if(this.offlineMessage) this.hideOfflineMessage();

        this.offlineMessage = this.alert.create({
            title: 'No internet connection',
            subTitle: 'This feature is unavailable until an internet connection is established',
            enableBackdropDismiss: false,
            buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: () => {this.hideOfflineMessage()}
            }]
        });

        this.offlineMessage.present();
    }

    private async getAppVersion(): Promise<string | number> {
        return await this.appVersion.getVersionNumber()
            .catch(err => { throw new Error(err); });
    }

    private async getBuild(): Promise<string | number> {
        return await this.appVersion.getVersionCode()
            .catch(err => { throw new Error(err); })
    }
}
