import { Component, ViewChild } from '@angular/core';
import { LoadingController, NavController, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { GoogleMap } from "../../components/google-maps";
import { FuelCardSite } from "../sites-nearby/sites-nearby";
import { FuelCardsService } from "../../providers/fuel-cards";
import { SiteSinglePage } from "../site-single/site-single";
import { GeolocationService } from '../../providers/geolocation';
import { SearchAreaService } from "../../providers/search-area";
import Place = google.maps.Place;

export interface SearchLocation { name: string, address: string, place_id: string|null }

@Component({
    selector: 'page-route-planner',
    templateUrl: 'route-planner.html'
})
export class RoutePlannerPage {
    @ViewChild('map') private map: GoogleMap;

    private sites: FuelCardSite[] = [];
    private sitesWithinSearchArea: FuelCardSite[] = [];

    public current_map_view_type: string;

    hgvOnly: boolean = false;
    showHGVFilter: boolean = false;
    showLocations: boolean = false;
    showInputs: boolean = true;
    routeSelected: boolean = false;
    startLocation: string;
    endLocation: string;
    searchResults: SearchLocation[] = [];
    startLocationPlaceId: string;
    endLocationPlaceId: string;
    currentlyEditing;
    awaitingDraw: boolean = false;

    constructor(
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        public fuelCards: FuelCardsService,
        private geoLocation: GeolocationService,
        public loadingCtrl: LoadingController,
        private searchArea: SearchAreaService,
        private toastCtrl: ToastController
    ) {
    }

    public async ngAfterViewInit() {
        let loading = this.loadingCtrl.create();
        loading.present();

        try {
            this.current_map_view_type = this.map.MAP_TYPES.ROAD;
            await this.map.ready();
            this.map.setOptions({'minZoom': 6});
            this.map.enableClustering();
            this.sites = await this.getAllSites();
            loading.dismiss();
        } catch (err) {
            console.error(err);
            loading.dismiss();
        }
    }

    public onLocationSearchKeyUp(keyUpEvent, startEnd) {
        this.currentlyEditing = startEnd;

        if (keyUpEvent.target.value.length) {
            this.map.placePrediction(keyUpEvent.target.value)
                .then((results: any[]) => {
                    if (results.length) {
                        this.searchResults = results;
                        this.showLocations = true;
                    } else {
                        this.showLocations = false;
                    }
                })
        } else {
            this.showLocations = false;
        }
    }

    public confirmAddress($address: SearchLocation, $startEnd) {
        this.addAddress($address, $startEnd);
        this.showLocations = false;
    }

    public addAddress($address: SearchLocation, $startEnd) {
        const { place_id } = $address;

        if ($startEnd == 'start') {
            this.startLocation = $address.name;
            this.startLocationPlaceId = place_id || null;
        } else {
            this.endLocation = $address.name;
            this.endLocationPlaceId = place_id || null;
        }

        if (this.startLocation && this.endLocation) {
            let loading = this.loadingCtrl.create();
            loading.present();

            this.showLocations = false;
            this.clearMap();
            this.displayRoute()
                .then(() => loading.dismiss())
        }
    }

    private async displayRoute() {
        const getSitesWithinSearchArea = searchArea => this.sites.filter(fuelPoint => {
            const latLng = new google.maps.LatLng(fuelPoint.latitude, fuelPoint.longitude);
            return google.maps.geometry.poly.containsLocation(latLng, searchArea);
        });

        let start: string|Place = this.startLocationPlaceId ? { placeId: this.startLocationPlaceId } : this.startLocation;
        let end: string|Place = this.endLocationPlaceId ? { placeId: this.endLocationPlaceId } : this.endLocation;

        await this.map.generateRoute(start, end)
            .then((route: google.maps.Polyline) => this.searchArea.fromPolyline(route, 2))
            .then(searchArea => getSitesWithinSearchArea(searchArea))
            .then(sites => {
                this.sitesWithinSearchArea = sites;
                this.sitesWithinSearchArea.map(fuelPoint => this.addMarkerForSite(fuelPoint));
                this.routeSelected = true;
                this.showHGVFilter = true;
            })
            .catch(() => {
                const alert = this.alertCtrl.create({
                    title: 'An error has occurred',
                    message: 'Sorry, but an error occurred while attempting to generate the route. Would you like to try again?',
                    buttons: [{
                        text: 'Yes',
                        handler: () => {
                            this.showInputs = true;
                        }
                    }, {
                        text: 'No',
                        handler: () => {
                            this.navCtrl.pop();
                        }
                    }]
                });
                alert.present();
            });
    }

    public addMarkerForSite(site): google.maps.Marker {
        if (site.marker) return;

        let marker = this.map.addMarker({
            icon: '/assets/imgs/map-marker-red.svg',
            position: {
                lat: site.latitude,
                lng: site.longitude
            },
            title: site.name,
        });

        marker.addListener('click', () => this.showSite(site));

        site.marker = marker;

        return marker;
    }

    public swapLocations() {
        if (this.startLocation == null || this.endLocation == null) {
            let alert = this.alertCtrl.create({
                title: 'Missing Location',
                subTitle: 'Please add your start and end addresses.',
                buttons: ['Close']
            });
            alert.present();
        } else {
            let newStart = this.endLocation;
            let newEnd = this.startLocation;
            this.startLocation = newStart;
            this.endLocation = newEnd;
            this.clearMap();
            this.displayRoute();
        }
    }

    private clearMap(): void {
        this.routeSelected = false;
        this.clearMarkersForSites();
        this.map.clearStartEndMarkers();

        if (this.map.searchArea !== undefined) {
            this.map.searchArea.setMap(null);
        }

        if (this.map.route !== undefined) {
            this.map.route.setMap(null);
        }

        this.sitesWithinSearchArea = [];
    }

    public clearMarkerForSite(site: FuelCardSite) {
        if (!site.marker) return;
        this.map.clearMarker(site.marker);

        site.marker = null;
    }

    public clearMarkersForSites(sites?: FuelCardSite[]) {
        if (!sites) sites = this.sites;

        for (let site of sites) {
            this.clearMarkerForSite(site);
        }
    }

    public getAllSites(): Promise<FuelCardSite[]> {
        return new Promise<FuelCardSite[]>((resolve, reject) => {
            this.fuelCards.getAllSites().subscribe(sites => {
                resolve(FuelCardSite.createMany(sites) as FuelCardSite[]);
            }, error => {
                reject(error);
            });
        });
    }

    public showSite(site: FuelCardSite) {
        this.navCtrl.push(SiteSinglePage, {
            site: site,
            destination: this.endLocation
        });
    }

    public onHgvOnlyChange(): void {
        this.sitesWithinSearchArea.map(site => {
            if (this.hgvOnly === true && site.hgv === false) {
                this.clearMarkerForSite(site);
            } else {
                this.addMarkerForSite(site);
            }
        })
    }

    public onCentreClick(): void {
        let loading = this.loadingCtrl.create();
        loading.present();

        this.geoLocation.getCurrentPosition()
            .then(location => new google.maps.LatLng(location.coords.latitude, location.coords.longitude))
            .then(location => {
                this.map.setCenter(location);
                loading.dismiss();
            })
            .catch(err => {
                let toast = this.toastCtrl.create({
                    message: 'Unable to determine your current location.',
                    duration: 3000,
                });

                loading.dismiss();
                toast.present();
            });
    }

    displayRoadView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.ROAD) return;

        this.current_map_view_type = this.map.MAP_TYPES.ROAD;
        this.map.displayRoadView();
    }

    displaySatelliteView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.SATELLITE) return;

        this.current_map_view_type = this.map.MAP_TYPES.SATELLITE;
        this.map.displaySatelliteView();
    }

    displayHybridView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.HYBRID) return;

        this.current_map_view_type = this.map.MAP_TYPES.HYBRID;
        this.map.displayHybridView();
    }

}
