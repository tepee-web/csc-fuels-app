import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { FuelCardSite } from '../../providers/fuel-cards/site';
import { GoogleMap } from '../../components/google-maps';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

@Component({
    selector: 'page-site-single',
    templateUrl: 'site-single.html'
})
export class SiteSinglePage {

    @ViewChild('map') private map: GoogleMap;

    public marker: google.maps.Marker;
    public site: FuelCardSite;
    public destination;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private launchNavigator: LaunchNavigator,
    ) {
        this.site = navParams.get('site');
        this.destination = navParams.get('destination') || false;
    }

    public async ngAfterViewInit() {
        await this.map.ready();

        let position = new google.maps.LatLng(this.site.latitude, this.site.longitude);

        this.map.setCenter(position);
        this.map.setZoom(14);

        this.marker = this.map.addMarker({
            icon: '/assets/imgs/map-marker-red.svg',
            position: position,
            title: this.site.name
        });
    }

    public ngOnDestroy() {
        this.marker.unbindAll();
        this.marker.setMap(null);
        this.marker = null;
        this.map = null;
    }

    public launchNativeNavigation() {
        let route = `${this.site.latitude},${this.site.longitude}`;

        if (this.destination) route += `+to:${this.destination}`;

        let options: LaunchNavigatorOptions = {};
        this.launchNavigator.navigate(route, options);
    }
}
