import { Component, ViewChild } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from 'ionic-angular';
import { debounceTime, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';
import { GoogleMap } from '../../components/google-maps';
import { FuelCardsService } from '../../providers/fuel-cards';
import { FuelCardSite as BaseFuelCardSite } from '../../providers/fuel-cards/site';
import { SiteSinglePage } from '../site-single/site-single';
import { GeolocationService } from '../../providers/geolocation';

const USER_LOCATION_FAILED_MSG = 'Failed to retrieve user location';

export class FuelCardSite extends BaseFuelCardSite {
    public marker: google.maps.Marker;

    public fill(data: any) {
        super.fill(data);
        this.marker = data.marker || null;
    }
}

@Component({
    selector: 'page-sites-nearby',
    templateUrl: 'sites-nearby.html'
})
export class SitesNearbyPage {

    @ViewChild('map') private map: GoogleMap;

    private drawing: boolean = false;
    private sites: FuelCardSite[] = [];

    public awaitingDraw: boolean = false;
    public hgvOnly: boolean = false;

    public onBoundsChanged: Subscription;
    public onPositionChanged: Subscription;

    public userLocationMarker: google.maps.Marker;

    public current_map_view_type: string;

    showLocations = false;
    searchLocation;
    className: string;
    searchResults = [];
    searchPhrase: string;

    constructor(
        public alertCtrl: AlertController,
        public fuelCards: FuelCardsService,
        public geolocation: GeolocationService,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public toastCtrl: ToastController,
    ) {

    }

    public async ngAfterViewInit() {
        let loading = this.loadingCtrl.create();
        loading.present();

        try {
            this.current_map_view_type = this.map.MAP_TYPES.ROAD;
            await this.init();
        } catch (error) {
            const msg = error.message === USER_LOCATION_FAILED_MSG
                ? 'Sorry, but an error occurred while attempting to fetch your location. Please ensure location services are turned on. Would you like to retry?'
                : null;

            this.showInitError(msg);
        } finally {
            loading.dismiss();
        }
    }

    public ngOnDestroy() {
        if (this.onBoundsChanged && !this.onBoundsChanged.closed) this.onBoundsChanged.unsubscribe();
        if (this.onPositionChanged && !this.onPositionChanged.closed) this.onPositionChanged.unsubscribe();
        this.clearMarkersForSites();
    }

    public addMarkerForSite(site: FuelCardSite): google.maps.Marker {
        if (site.marker) return;

        let marker = this.map.addMarker({
            icon: '/assets/imgs/map-marker-red.svg',
            position: {
                lat: site.latitude,
                lng: site.longitude
            },
            title: site.name,
        });

        marker.addListener('click', () => this.showSite(site));

        site.marker = marker;

        return marker;
    }

    public clearMarkerForSite(site: FuelCardSite) {
        if (!site.marker) return;
        this.map.clearMarker(site.marker);
        site.marker = null;
    }

    public clearMarkersForSites(sites?: FuelCardSite[]) {
        if (!sites) sites = this.sites;

        for (let site of sites) {
            this.clearMarkerForSite(site);
        }
    }

    public drawSites(sites?: FuelCardSite[]) {
        if (this.drawing) return;

        this.drawing = true;

        if (!sites) sites = this.sites;

        let bounds = this.map.getBounds();
        let extendedBounds = this.getExtendedBoundsByPercent(bounds, 20);

        for (let site of this.sites) {
            if (this.hgvOnly && !site.hgv || !site.isWithinBounds(extendedBounds)) {
                this.clearMarkerForSite(site);
            } else {
                this.addMarkerForSite(site);
            }
        }

        this.drawing = false;
    }

    public getAllSites(): Promise<FuelCardSite[]> {
        return new Promise<FuelCardSite[]>((resolve, reject) => {
            this.fuelCards.getAllSites().subscribe(sites => {
                resolve(FuelCardSite.createMany(sites) as FuelCardSite[]);
            }, error => {
                reject(error);
            });
        });
    }

    public getExtendedBoundsByPercent(bounds: google.maps.LatLngBounds, percent: number) {
        let northEast = bounds.getNorthEast();
        let southWest = bounds.getSouthWest();

        let latSpan = northEast.lat() - southWest.lat();
        let lngSpan = northEast.lng() - southWest.lng();

        let multiplier = 2 / percent;

        return new google.maps.LatLngBounds(
            new google.maps.LatLng(southWest.lat() - (latSpan * multiplier), southWest.lng() - (lngSpan * multiplier)),
            new google.maps.LatLng(northEast.lat() + (latSpan * multiplier), northEast.lng() + (lngSpan * multiplier)),
        );
    }

    public getSitesWithinBounds(bounds: google.maps.LatLngBounds, sites?: FuelCardSite[]) {
        if (!sites) sites = this.sites;

        let results: FuelCardSite[] = [];

        for (let site of sites) {
            if (site.isWithinBounds(bounds)) results.push(site);
        }

        return results;
    }

    public async init() {
        await Promise.all([
            this.map.ready(),
            this.map.setZoom(12),
            this.showMarkerForUser(),
            this.sites = await this.getAllSites(),
        ]);

        this.map.enableClustering();

        this.drawSites();

        this.onBoundsChanged = this.map.boundsChanged.pipe(
            tap(() => this.awaitingDraw = true),
            debounceTime(300)
        ).subscribe(bounds => {
            this.awaitingDraw = false;
            this.drawSites();
        });
    }

    public async showMarkerForUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (typeof this.userLocationMarker !== "undefined") this.map.clearMarker(this.userLocationMarker);

            this.geolocation.getCurrentPosition()
                .then(location => new google.maps.LatLng(location.coords.latitude, location.coords.longitude))
                .then(position => {
                    this.userLocationMarker = this.map.addMarker({
                        icon: '/assets/imgs/map-marker-user.svg',
                        position: position,
                        title: ''
                    });

                    this.map.setCenter(position);

                    resolve(position);
                })
                .catch ((e: Error) => reject(new Error(USER_LOCATION_FAILED_MSG)))
        });
    }

    public onHgvOnlyChange() {
        this.drawSites();
    }

    public showInitError(msg?: string|null) {
        let alert = this.alertCtrl.create({
            title: 'An error has occurred',
            message: msg || 'Sorry, but an error occurred while attempting to fetch and display our sites. Would you like to retry?',
            buttons: [{
                text: 'Yes',
                handler: () => {
                    this.ngOnDestroy();
                    this.ngAfterViewInit();
                }
            }, {
                text: 'No',
                handler: () => {
                    this.navCtrl.pop();
                }
            }]
        });
        alert.present();
    }

    public showSite(site: FuelCardSite) {
        this.navCtrl.push(SiteSinglePage, {
            site: site
        });
    }

    public async onTargetClick() {
        let loading = this.loadingCtrl.create();
        loading.present();

        try {
            this.showMarkerForUser()
                .then(() => loading.dismiss());
        } catch (err) {
            let toast = this.toastCtrl.create({
                message: 'Unable to determine your current location',
                duration: 3000,
            });

            loading.dismiss();
            toast.present();
        }
    }

    public onLocationSearchKeyUp(keyUpEvent): void {
        if (keyUpEvent.target.value.length) {
            this.map.placePrediction(keyUpEvent.target.value)
                .then((results: Array<any>) => {
                    if (results.length) {
                        this.searchResults = results;
                        this.showLocations = true;
                    } else {
                        this.showLocations = false;
                    }
                })
        } else {
            this.showLocations = false;
        }
    }

    public selectPlaceSuggestion(address: string): void {
        let loading = this.loadingCtrl.create();
        loading.present();

        this.searchPhrase = address;
        this.showLocations = false;

        try {
            this.map.getLatLngForPlace(address)
            .then(latLng => {
                this.map.setCenter(latLng);
                loading.dismiss();
            });
        } catch (e) {
            loading.dismiss();
        }
    }

    displayRoadView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.ROAD) return;

        this.current_map_view_type = this.map.MAP_TYPES.ROAD;
        this.map.displayRoadView();
    }

    displaySatelliteView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.SATELLITE) return;

        this.current_map_view_type = this.map.MAP_TYPES.SATELLITE;
        this.map.displaySatelliteView();
    }

    displayHybridView() {
        if (this.current_map_view_type === this.map.MAP_TYPES.HYBRID) return;

        this.current_map_view_type = this.map.MAP_TYPES.HYBRID;
        this.map.displayHybridView();
    }
}
