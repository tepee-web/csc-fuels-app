import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { map, tap } from "rxjs/operators";
import { FuelCardSite } from "./fuel-cards/site";

@Injectable()
export class FuelCardsService {

    private sites: FuelCardSite[];
    private src = "https://www.cscfuelcards.com/wp-content/themes/Fuelcards/inc/fuel_card.json";

    constructor(
        private http: Http,
    ) {

    }

    public fetchAllSites(): Observable<FuelCardSite[]> {
        let timestamp = new Date().getTime();
        return this.http.get(this.src + '?timestamp=' + timestamp).pipe(
            map(response => response.json()),
            map(response => FuelCardSite.createMany(response)),
        );
    }

    public getAllSites(): Observable<FuelCardSite[]> {
        if (this.sites) {
            return new Observable(observer => {
                observer.next(this.sites);
                observer.complete();
            });
        }

        return this.fetchAllSites().pipe(
            tap(sites => this.sites = sites)
        );
    }

    public getAllWithinBounds(bounds: google.maps.LatLngBounds, sites?: FuelCardSite[]) {
        if (!sites) sites = this.sites;

        let results = [];

        for (let site of sites) {
            if (site.isWithinBounds(bounds)) results.push(site);
        }

        return results;
    }

}
