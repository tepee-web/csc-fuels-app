export class FuelCardSite {

    public name: string;
    public addr1: string;
    public addr2: string;
    public city: string;
    public county: string;
    public postcode: string;
    public phone: string;
    public band: string;
    public opening: string;
    public hgv: boolean;
    public latitude: number;
    public longitude: number;
    public country: string;

    public static createMany(data: any[]): FuelCardSite[] {
        let sites: FuelCardSite[] = [];
        for (let item of data) {
            sites.push(FuelCardSite.createOne(item));
        }
        return sites;
    }

    public static createOne(data: any): FuelCardSite {
        let site = new FuelCardSite();
        site.fill(data);
        return site;
    }

    public fill(data: any) {
        this.name = data.name || data.siteName || null;
        this.addr1 = data.addr1 || null;
        this.addr2 = data.addr2 || null;
        this.city = data.city || null;
        this.county = data.county || null;
        this.postcode = data.postcode || null;
        this.band = data.band || null;
        this.phone = data.phone || null;
        this.opening = data.opening || null;
        this.latitude = parseFloat(data.latitude);
        this.longitude = parseFloat(data.longitude);
        this.country = data.country || null;

        this.setHgv(data.hgv);
    }

    public getAddressParts(): string[] {
        let parts: string[] = [];
        if (this.addr1) parts.push(this.addr1);
        if (this.addr2) parts.push(this.addr2);
        if (this.city) parts.push(this.city);
        if (this.county) parts.push(this.county);
        if (this.country) parts.push(this.country);
        if (this.postcode) parts.push(this.postcode);
        return parts;
    }

    public getAddressString(separator: string = ', ') {
        return this.getAddressParts().join(separator);
    }

    public setHgv(value: string): void;
    public setHgv(value: boolean): void;
    public setHgv(value: any): void {
        if (typeof value == 'string') this.hgv = value.toUpperCase() == 'YES' ? true : false;
        else this.hgv = value;
    }

    public isWithinBounds(bounds: google.maps.LatLngBounds): boolean {
        let northEast = bounds.getNorthEast();
        let southWest = bounds.getSouthWest();

        if (this.latitude > northEast.lat()) return false;
        if (this.latitude < southWest.lat()) return false;

        if (this.longitude > northEast.lng()) return false;
        if (this.longitude < southWest.lng()) return false;

        return true;
    }

}
