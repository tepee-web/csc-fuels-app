import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Geolocation, GeolocationOptions } from "@ionic-native/geolocation";
import { Platform } from "ionic-angular";

@Injectable()
export class GeolocationService {

    constructor(public geolocation: Geolocation, private platform: Platform) {}

    public getCurrentPosition(): Promise<Position> {
        return new Promise((resolve, reject) => {
            this.platform.ready()
                .then(() => {
                    const options: GeolocationOptions = { enableHighAccuracy: true, timeout: 30000 };
                    this.geolocation.getCurrentPosition(options)
                        .then(location => resolve(location))
                        .catch(err => reject(err));
                }).catch(err => reject(err));
        });
    }

    public watchPosition(): Observable<Position> {
        return new Observable<Position>(observer => {
            let watchId = navigator.geolocation.watchPosition(
                success => observer.next(success),
                error => observer.error(error)
            );

            return () => {
                navigator.geolocation.clearWatch(watchId);
            }
        });
    }

}
