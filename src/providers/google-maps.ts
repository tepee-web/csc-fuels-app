import { Injectable } from "@angular/core";
import * as RandomString from 'randomstring'

@Injectable()
export class GoogleMapsService {

    private isReady: boolean = false;

    private readyCallbacks: Function[] = [];
    private initCallback: string = null;
    private key: string = 'AIzaSyChROQCzEWnutrKOeh1CNnw94rxTuoaHIM';

    constructor(

    ) {

    }

    private addElementToHead(element: HTMLElement) {
        let head = document.getElementsByTagName('head').item(0);
        if (!head) return;
        head.appendChild(element);
    }

    public createScriptElement(): HTMLScriptElement {
        let src = new URL('https://maps.googleapis.com/maps/api/js');

        src.searchParams.append('key', this.key);
        src.searchParams.append('libraries', 'places');
        src.searchParams.append('callback', this.initCallback);

        let script = document.createElement('script');
        script.src = src.href;

        return script;
    }

    public init() {
        this.setUpInitCallback();

        let scriptElement = this.createScriptElement();

        this.addElementToHead(scriptElement);
    }

    private handleInitCallback() {
        this.isReady = true;

        for (let callback of this.readyCallbacks) {
            callback();
        }
    }

    private setUpInitCallback() {
        do {
            this.initCallback = 'initGoogleMaps_' + RandomString.generate(8);
        } while (window[this.initCallback] !== undefined);

        window[this.initCallback] = () => {
            this.handleInitCallback();
        }
    }

    public ready(callback: Function) {
        if (!this.isReady) {
            this.readyCallbacks.push(callback);
        } else {
            callback();
        }
    }

}
