import { Injectable } from '@angular/core';
import * as jsts from 'jsts';
@Injectable()
export class SearchAreaService {
    constructor() {}

    public fromPolyline(route: google.maps.Polyline, radiusInMiles = 10): google.maps.Polygon {
        const parts = this.createPolygonsForWaypoints(route, radiusInMiles);
        return this.union(parts);
    }

    private createPolygonsForWaypoints(route: google.maps.Polyline, radiusInMiles): Array<google.maps.Polygon> {
        const parts = [];
        const milesToMetres = (miles: number) => miles * 1609.34;
        const RANGE = milesToMetres(radiusInMiles);

        const createPolygon = (heading, start, end) => {
            const NW = -45, NE = 45, SW = -135, SE = 135;

            let furthestNW;
            let furthestSW;
            let furthestNE;
            let furthestSE;

            if (heading >= 0 && heading <= 180) {
                furthestNW = google.maps.geometry.spherical.computeOffset(start, RANGE, NW);
                furthestSW = google.maps.geometry.spherical.computeOffset(start, RANGE, SW);
                furthestNE = google.maps.geometry.spherical.computeOffset(end, RANGE, NE);
                furthestSE = google.maps.geometry.spherical.computeOffset(end, RANGE, SE);
            } else {
                furthestNW = google.maps.geometry.spherical.computeOffset(end, RANGE, NW);
                furthestSW = google.maps.geometry.spherical.computeOffset(end, RANGE, SW);
                furthestNE = google.maps.geometry.spherical.computeOffset(start, RANGE, NE);
                furthestSE = google.maps.geometry.spherical.computeOffset(start, RANGE, SE);
            }

            return new google.maps.Polygon({
                paths: [furthestNW, furthestNE, furthestSE, furthestSW, furthestNW]
            });
        };

        for (let curr = 0, next = 1; next < route.getPath().getLength(); curr++, next++) {
            const heading = google.maps.geometry.spherical.computeHeading(route.getPath().getAt(curr), route.getPath().getAt(next));
            parts.push(createPolygon(heading, route.getPath().getAt(curr), route.getPath().getAt(next)));
        }

        return parts;
    }

    private union(polygons: Array<google.maps.Polygon>): google.maps.Polygon {
        const googleMaps2JSTS = function (boundaries) {
            const coordinates = [];
            for (let i = 0; i < boundaries.getLength(); i++) {
                coordinates.push(new jsts.geom.Coordinate(
                    boundaries.getAt(i).lat(), boundaries.getAt(i).lng()));
            }
            return coordinates;
        };

        const jsts2googleMaps = function (geometry) {
            const coordArray = geometry.getCoordinates();
            const GMcoords = [];
            for (let i = 0; i < coordArray.length; i++) {
                GMcoords.push(new google.maps.LatLng(coordArray[i].x, coordArray[i].y));
            }
            return GMcoords;
        };

        const geometryFactory = new jsts.geom.GeometryFactory();

        // @ts-ignore
        let poly = geometryFactory.createPolygon(geometryFactory.createLinearRing(googleMaps2JSTS(polygons[0].getPath())));

        for (let i = 1; i < polygons.length; i++) {
            // @ts-ignore
            let jstsPolygon = geometryFactory.createPolygon(geometryFactory.createLinearRing(googleMaps2JSTS(polygons[i].getPath())));
            jstsPolygon.normalize();
            // @ts-ignore
            poly = poly.union(jstsPolygon);
        }

        const outputPath = jsts2googleMaps(poly);

        return new google.maps.Polygon({
            paths: outputPath
        });
    }
}
